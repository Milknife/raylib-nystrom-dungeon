# Raylib Nystrom Dungeon

A C# | Raylib implementation of the dungeon generation algorithm as explained found [https://journal.stuffwithstuff.com/2014/12/21/rooms-and-mazes/](https://journal.stuffwithstuff.com/2014/12/21/rooms-and-mazes/)

Note: User will probably need to copy the Raylib.dll into their debug and release folders