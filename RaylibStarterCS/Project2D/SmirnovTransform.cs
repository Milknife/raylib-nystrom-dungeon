﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ValueGridUtilities;

/// <summary>
/// Sample specialisation of the smirnov transform applied to dungeon generation
/// Converted from generic cpp implementation found at
/// https://bitbucket.org/Milknife/smirnov_transform/src/master/
/// to C#
/// 
/// Quick and dirty
/// As such, data hiding / protection has been ignored.
/// </summary>

namespace Project2D
{

    public struct SmirnovPair
    {
        /// <summary>
        /// The X axis value of the PDF (Room bounds)
        /// </summary>
        public readonly ValueGrid Extents;
        /// <summary>
        /// The y axis value of the PDF (Relative membership)
        /// </summary>
        public readonly float Frequency;
        /// <summary>
        /// Normalized Membership of the PDF
        /// </summary>
        public double Membership;

        /// <summary>
        /// Should only be using this constructor
        /// </summary>
        /// <param name="a_Extents">Room Bounds</param>
        /// <param name="a_Frequency">Absolute occurence</param>
        public SmirnovPair(ValueGrid a_Extents, float a_Frequency)
        {
            Extents = a_Extents;
            Frequency = a_Frequency;
            Membership = 0;
        }

        /// <summary>
        ///  Should only be set by a transformation
        /// </summary>
        /// <param name="membership"></param>
        public void SetMembership(double membership)
        {
            Membership = membership;
        }
    }

    /// <summary>
    /// Transformation object for gathering and processing pairs
    /// </summary>
    public class SmirnovTransform
    {
        /// <summary>
        /// Final selection list
        /// </summary>
        List<SmirnovPair> valid_data;

        /// <summary>
        /// Weighted search space
        /// </summary>
        double[] summised_membership_values;
        
        /// <summary>
        /// C# RNG object
        /// </summary>
        Random RNG;

        /// <summary>
        /// Hand it a list of data for processing before sampling
        /// IMPORTANT: From here on was a cpp dump from the original,
        /// with only minimum required fixes for C#
        /// </summary>
        /// <param name="a_Rooms"></param>
        public SmirnovTransform(List<SmirnovPair> a_Rooms)
        {
            // Init. RNG
            RNG = new Random();

            //Copy all non-zeros
            valid_data = new List<SmirnovPair>();
            foreach (SmirnovPair pair in a_Rooms)
            {
                if (pair.Frequency >= 0)
                {
                    valid_data.Add(pair);
                }
            }

            // The total sum of all T_y in order to calculate normalized membership
            double summation_y = 0;
            foreach (var room in valid_data)
                summation_y += room.Frequency;

            // Calculate the total membership from zero to any index n
            double[] normalized_y_axis_values = new double[valid_data.Count];

            // Iterate over all data...
            for (int index = 0; index < valid_data.Count; ++index)
            {
                // ...calculating normalized membership
                normalized_y_axis_values[index] =
                    valid_data[index].Frequency / summation_y;
                // Store in each

                valid_data[index].SetMembership(normalized_y_axis_values[index]);
            }

            // Initialize the container for our sample search space
            summised_membership_values = new double[valid_data.Count];

            // Store the accumulated membership to each index
            summised_membership_values[0] = normalized_y_axis_values[0];
            for (int index = 1; index < valid_data.Count; ++index)
            {
                summised_membership_values[index] =
                    summised_membership_values[index - 1] + normalized_y_axis_values[index];
            }
        }

        public ValueGrid Sample()
        {
            // Our normal range sample point
            double ratio = RNG.NextDouble();

            // Indicies for binary search to locate smirnov_pair that contains the
            // random membership ratio from above.
            int low = 0;
            int high = valid_data.Count - 1;
            int mid = 0;
            int found_index = 0;

            bool found = false;

            // While not found and not an error...
            while (low <= high && !found)
            {
                // Split the seach space
                mid = (low + high) / 2;

                // Low-half check
                if (ratio < summised_membership_values[mid])
                {
                    // It was the first element all along
                    if (mid == 0)
                    {
                        found_index = mid;
                        found = true;
                    }
                    else // Bring down the high index
                    {
                        high = mid - 1;
                    }
                }
                else if (ratio >= summised_membership_values[mid])
                {
                    // (Extreme) Rare exact hit
                    if (ratio == summised_membership_values[mid])
                    {
                        found_index = mid;
                        found = true;
                    }
                    // Move up the low index
                    low = mid + 1;
                    // and check.
                    if (ratio <= summised_membership_values[low])
                    {
                        found_index = low;
                        found = true;
                    }
                }

            }
            if (!found)
            {
                found_index = mid;
            }
            return valid_data[found_index].Extents;
        }
    }
}
