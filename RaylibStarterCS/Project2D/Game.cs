﻿using System;
using System.Diagnostics;
using Raylib;
using static Raylib.Raylib;
using ValueGridUtilities;

namespace Project2D
{
    class Game
    {
        /// <summary>
        /// Stopwatch for deltaTime calculations
        /// </summary>
        Stopwatch stopwatch = new Stopwatch();

        /// <summary>
        /// Time since program instantiation till this frame
        /// </summary>
        private long currentTime = 0;

        /// <summary>
        /// Time since program instantiation till previous frame
        /// </summary>
        private long lastTime = 0;

        /// <summary>
        /// Delta time scoped to class so it can also be used in Draw()
        /// </summary>
        private float deltaTime = 0;

        /// <summary>
        /// Dungeon generator object
        /// </summary>
        Dungeon.Nystrom.Generator nystromDungeon;

        /// <summary>
        /// Dungeon renderer object
        /// </summary>
        Dungeon.Nystrom.Renderer regionGridRenderer;

        /// <summary>
        /// Toggle to have dungeon metadata displayed
        /// </summary>
        private bool isDisplayingData = false;

        /// <summary>
        /// Dungion configuration data string for display on demand
        /// </summary>
        private string dungeonData = "";

        /// <summary>
        /// Toggle to have dungeon metadata displayed
        /// </summary>
        private bool isDisplayingInstructions = true;

        /// <summary>
        /// Instuction string for display on demand
        /// </summary>
        private string instructions =
            "'F1' Toggle Help | " + 
            "'I' Toggle Data\n" +
            "'F5' Save | " + 
            "'F9' Load most recent\n" +
            "'RMB' Sample 'LMB' Paint\n" +
            "'G' Generate new parameters\n" +
            "'H' Regenerate with parameters\n" +
            "'P' Screenshot\n" +
            "'F' Toggle Fullscreen\n" +
            "'Space' Toggle HD 1080|768";

        /// <summary>
        /// For runtime filename creation and persistance
        /// </summary>
        private string fileName;

        /// <summary>
        /// Tile type sampled/painted
        /// </summary>
        private int tileID;

        /// <summary>
        /// Region value sampled/painted
        /// </summary>
        private int regionID;

        /// <summary>
        /// Offsets to apply from the bottom left to frame the infomation strings
        /// </summary>
        private Rectangle UIOffsets = new Rectangle(332,264,308,244);

        /// <summary>
        /// Color of the UI backing pane
        /// </summary>
        private Color UIPanelColor = new Color(0, 0, 0, 127);

        /// <summary>
        /// Text offset also from the bottom left corner
        /// </summary>
        private Vector2 TextOffset = new Vector2(324, 256);

        /// <summary>
        /// Font size of text
        /// </summary>
        private readonly int TextSize = 16;

        private readonly Vector2 HD1080 = new Vector2(1920, 1080);
        private readonly Vector2 HD768 = new Vector2(1366, 768);

        /// <summary>
        /// Init exists to construct the game 
        ///   after openGL / Raylib has had time to initialise
        /// </summary>
        public void Init()
        {
            // Start our timer
            stopwatch.Start();
            lastTime = stopwatch.ElapsedMilliseconds;

            // Prepare the renderer
            regionGridRenderer = new Dungeon.Nystrom.Renderer();

            // Create the dungeon
            nystromDungeon = new Dungeon.Nystrom.Generator();
            nystromDungeon.Generate();
            PopulateDungeonDataString();
        }

        /// <summary>
        /// Called each frame, updates timers and executes based on input
        /// </summary>
        public void Update()
        {
            // Update delta time
            lastTime = currentTime;
            currentTime = stopwatch.ElapsedMilliseconds;
            deltaTime = (currentTime - lastTime) / 1000.0f;

            int screenMouseX = GetMouseX();
            int screenMouseY = GetMouseY();
            int tileXCoord = (int)(screenMouseX / regionGridRenderer.SizeXY);
            int tileYCoord = (int)(screenMouseY / regionGridRenderer.SizeXY);
            Location mouseLocation = new Location(tileXCoord, tileYCoord);

            /** MOUSE INPUT **/
            // First frame Left mouse is pressed
            if (IsMouseButtonPressed(MouseButton.MOUSE_LEFT_BUTTON))
            {
                // Nothing on first frame of click atm
            }
            // Every following frame left mouse is held
            else if (IsMouseButtonDown(MouseButton.MOUSE_LEFT_BUTTON))
            {
                if (Location.BoundsCheck(mouseLocation, Location.Origin, nystromDungeon.Bounds))
                {
                    // Paint!
                    nystromDungeon.TileGrid[mouseLocation] = tileID;
                    nystromDungeon.RegionGrid[mouseLocation] = regionID;
                }
            }

            // First frame right mouse is pressed
            if (IsMouseButtonPressed(MouseButton.MOUSE_RIGHT_BUTTON))
            {
                if (Location.BoundsCheck(mouseLocation, Location.Origin, nystromDungeon.Bounds))
                {
                    // Sample!
                    tileID = nystromDungeon.TileGrid[mouseLocation];
                    regionID = nystromDungeon.RegionGrid[mouseLocation];
                }
            }
            // Every following frame right mouse is held
            else if (IsMouseButtonDown(MouseButton.MOUSE_RIGHT_BUTTON))
            {
                // Held is nothing special
            }

            /** KEYBOARD INPUT **/
            // Toggle dungeon info
            if (IsKeyPressed(KeyboardKey.KEY_I))
            {
                isDisplayingData = !isDisplayingData;
                if (isDisplayingInstructions)
                {
                    isDisplayingInstructions = !isDisplayingInstructions;
                }
            }
            // Toggle instructions
            if (IsKeyPressed(KeyboardKey.KEY_F1))
            {
                isDisplayingInstructions = !isDisplayingInstructions;
                if (isDisplayingData)
                {
                    isDisplayingData = !isDisplayingData;
                }
            }

            /** Display configs for HD and Laptop **/
            // Toggles full screen
            if (IsKeyPressed(KeyboardKey.KEY_F))
            {
                ToggleFullscreen();
            }
            // Toggles resolution
            if (IsKeyPressed(KeyboardKey.KEY_SPACE))
            {
                if (GetScreenWidth() != HD768.x)
                {
                    SetWindowSize((int)HD768.x, (int)HD768.y);
                    SetWindowPosition((int)(HD1080.x - HD768.x) / 2, (int)(HD1080.y - HD768.y) / 2);
                }
                else
                {
                    SetWindowPosition(0, 0);
                    SetWindowSize((int)HD1080.x, (int)HD1080.y);
                }

            }

            // Generate with new random settings
            if (IsKeyPressed(KeyboardKey.KEY_G))
            {
                Random rng = new Random();

                // Gladly randomise everything.
                nystromDungeon.Bounds = new ValueGridUtilities.Location(rng.Next(1920 / 16), rng.Next(1080 / 16));
                nystromDungeon.MinimumRoomExtent = rng.Next(7);
                nystromDungeon.MaximumRoomExtent = rng.Next(20);
                nystromDungeon.MinimumBonusExtent = rng.Next(3);
                nystromDungeon.MaximumBonusExtent = rng.Next(20);
                nystromDungeon.BonusExtentChance = (float)rng.NextDouble();
                nystromDungeon.MaximumRoomPlacementAttempts = rng.Next(100);
                nystromDungeon.MaximumRoomCount = rng.Next(100);
                nystromDungeon.BonusConnectionChance = (float)rng.NextDouble();
                nystromDungeon.NextRNGSeed = rng.Next();

                // Create
                nystromDungeon.Generate();
                PopulateDungeonDataString();
            }
            // Regenerate with current settings
            if (IsKeyDown(KeyboardKey.KEY_H))
            {
                nystromDungeon.Generate();
                PopulateDungeonDataString();
            }

            // Take a screenshot - Printscreen does not correctly poll :(
            if (IsKeyPressed(KeyboardKey.KEY_P) /*|| IsKeyPressed(KeyboardKey.KEY_PRINT_SCREEN)*/)
            {
                fileName = TimeAsFileName();
                TakeScreenshot(fileName + ".png");
            }
            // Save to file
            if (IsKeyPressed(KeyboardKey.KEY_F5))
            {
                // Save as previous.
                nystromDungeon.Save("Dungeon.dgn");
                    
                // And archive by date
                fileName = TimeAsFileName();
                nystromDungeon.Save(fileName + ".dgn");
                TakeScreenshot(fileName + ".png");
            }
            // Load from file
            if (IsKeyPressed(KeyboardKey.KEY_F9))
            {
                nystromDungeon.Load("Dungeon.dgn");
                PopulateDungeonDataString();
            }


        }

        /// <summary>
        /// Draw every tile in the dungeon
        /// </summary>
        public void Draw()
        {
            // GPU, good to go
            BeginDrawing();

            // Clear to black
            ClearBackground(Color.BLACK);

            // Render with a saturation of 0.5f (1.0f will be vibrant)
            regionGridRenderer.Draw(nystromDungeon, 0.5f);

            // Debug display data
            if (isDisplayingData)
            {
                DrawRectangle(
                    GetScreenWidth() - (int)UIOffsets.x,
                    GetScreenHeight() - (int)UIOffsets.y,
                    (int)UIOffsets.width,
                    (int)UIOffsets.height,
                    UIPanelColor);

                DrawText(
                    dungeonData,
                    GetScreenWidth() - (int)TextOffset.x,
                    GetScreenHeight() - (int)TextOffset.y,
                    TextSize,
                    Color.WHITE);
            }
            // Instructions on usage
            if (isDisplayingInstructions)
            {
                DrawRectangle(
                    GetScreenWidth() - (int)UIOffsets.x,
                    GetScreenHeight() - (int)UIOffsets.y,
                    (int)UIOffsets.width,
                    (int)UIOffsets.height,
                    UIPanelColor);

                DrawText(
                    instructions,
                    GetScreenWidth() - (int)TextOffset.x,
                    GetScreenHeight() - (int)TextOffset.y,
                    TextSize,
                    Color.WHITE);
            }

            // GPU, we are done now
            EndDrawing();
            
        }
        
        /// <summary>
        /// Creates a string in the format YYYYMMDDHHMMSS
        /// </summary>
        /// <returns></returns>
        private string TimeAsFileName()
        {
                DateTime fileTime = DateTime.Now;
                return 
                    fileTime.Year.ToString("D4") +
                    fileTime.Month.ToString("D2") +
                    fileTime.Day.ToString("D2") +
                    fileTime.Hour.ToString("D2") +
                    fileTime.Minute.ToString("D2") +
                    fileTime.Second.ToString("D2");
        }

        /// <summary>
        /// Populates Dungeon Data String
        /// </summary>
        private void PopulateDungeonDataString()
        {
            // For debug
            dungeonData =
                    "Seed: " + nystromDungeon.CurrentRNGSeed.ToString() + "\n" +
                    "Bounds: X " + nystromDungeon.Bounds.X + " | Y " + nystromDungeon.Bounds.Y + "\n" +
                    "Room Count: " + nystromDungeon.CurrentRoomCount + "\n" +
                    "Room Extents: Min. " + nystromDungeon.MinimumRoomExtent.ToString() + " | Max. " + nystromDungeon.MaximumRoomExtent.ToString() + "\n" +
                    "Room Bonus Extents: Min." + nystromDungeon.MinimumBonusExtent.ToString() + " | Max. " + nystromDungeon.MaximumBonusExtent.ToString() + "\n" +
                    "Bonus Extents Chance: " + nystromDungeon.BonusExtentChance.ToString() + "\n" +
                    "Max Room Count: " + nystromDungeon.MaximumRoomCount.ToString() + "\n" +
                    "Max Room Placement Attempts: " + nystromDungeon.MaximumRoomPlacementAttempts.ToString() + "\n" +
                    "Bonus Connector Chance: " + nystromDungeon.BonusConnectionChance.ToString() + "\n" +
                    "Creation Duration: " + nystromDungeon.CreationDuration.ToString();
        }

        /// <summary>
        /// Constructor left blank intentionally
        /// </summary>
        public Game() { }
        /// <summary>
        /// Shutdown to manually close of any files etc.
        /// </summary>
        public void Shutdown() { }

    }
}
