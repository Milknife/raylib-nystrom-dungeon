﻿using System;

/// <summary>
/// The lowests form of data representation for a two dimentional grid
/// before bitwise optimisation are needed.
/// </summary>
namespace ValueGridUtilities
{
    public struct Location : IEquatable<Location>
    {
        /// <summary>
        /// Location's X Co-ordinate
        /// </summary>
        public int X { get; set; }
        /// <summary>
        /// Location's Y Co-ordinate
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Location constructor must take values
        /// </summary>
        public Location(int a_x, int a_y) 
        {
            X = a_x;
            Y = a_y;
        }

        /// <summary>
        /// Location as a fixed {0,0}
        /// </summary>
        public static Location Origin { get; }

        /// <summary>
        /// Increment the location along X
        /// </summary>
        /// <returns>A new location with X incremented</returns>
        public Location NextX()
        {
            return new Location(X + 1, Y);
        }

        /// <summary>
        /// Decrement the location along Y
        /// </summary>
        /// <returns>A new location with Y decremented</returns>
        public Location PrevX()
        {
            return new Location(X - 1, Y);
        }
        /// <summary>
        /// Iterate the location along Y
        /// </summary>
        /// <returns>A new location with Y incremented</returns>
        public Location NextY()
        {
            return new Location(X, Y + 1);
        }

        /// <summary>
        /// Decrement the location along Y
        /// </summary>
        /// <returns>A new location with Y decremented</returns>
        public Location PrevY()
        {
            return new Location(X, Y - 1);
        }


        /// <summary>
        /// Check to see if the Location is valid within the given axis aligned bounds
        /// </summary>
        /// <param name="a_Location"></param>
        /// <param name="a_MaximumBounds"></param>
        /// <returns></returns>
        public static bool BoundsCheck(Location a_Location, Location a_MinimumBounds, Location a_MaximumBounds)
        {
            return (a_Location.X >= a_MinimumBounds.X &&
                    a_Location.Y >= a_MinimumBounds.Y &&
                    a_Location.X < a_MaximumBounds.X &&
                    a_Location.Y < a_MaximumBounds.Y);
        }

        #region Housekeeping
        /// <summary>
        /// Equality operator
        /// </summary>
        /// <param name="a_Location1">LHS of operand</param>
        /// <param name="a_Location2">RHS of operand</param>
        /// <returns>Member-wise value equality state</returns>
        public static bool operator ==(Location a_Location1, Location a_Location2)
        {
            return a_Location1.Equals(a_Location2);
        }

        /// <summary>
        /// Inequality operator
        /// </summary>
        /// <param name="a_Location1">LHS of operand</param>
        /// <param name="a_Location2">RHS of operand</param>
        /// <returns>Member-wise value inequality state</returns>
        public static bool operator !=(Location a_Location1, Location a_Location2)
        {
            return !a_Location1.Equals(a_Location2);
        }

        /// <summary>
        /// Object validity and member wise equality comparison
        /// </summary>
        /// <param name="a_Location">Location to compare against</param>
        /// <returns>True if both a location and memberwise equal</returns>
        public override bool Equals(object a_Location)
        {
            return a_Location is Location && Equals((Location)a_Location);
        }

        /// <summary>
        /// Member wise equality comparison
        /// </summary>
        /// <param name="a_Location">Location to compare against</param>
        /// <returns>True if memberwise equal</returns>
        public bool Equals(Location a_Location)
        { 
            return X == a_Location.X && Y == a_Location.Y;
        }

        /// <summary>
        /// Create a formated string of this location
        /// </summary>
        /// <returns>X, Y co-ordinates in as a set</returns>
        public override string ToString()
        {
            return "{" + X.ToString() + "," + Y.ToString() + "}";
        }

        /// <summary>
        /// Auto-generated MS Hashing funtion
        /// </summary>
        /// <returns>Hash value of this object</returns>
        public override int GetHashCode()
        {
            var hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }
        #endregion
    }

}

/// <summary>
/// Extension functions for the ValueGrid given the support struct Location.
/// </summary>
public partial class ValueGrid
{
    /// <summary>
    /// Prepare the rectangualr array of values
    /// </summary>
    /// <param name="a_Bounds">uint pair for array extents</param>
    public ValueGrid(ValueGridUtilities.Location a_Bounds) : this((uint)a_Bounds.X, (uint)a_Bounds.Y) { }

    /// <summary>
    /// A copy of maximum bounds of the value grid
    /// </summary>
    public ValueGridUtilities.Location Bounds
    {
        get { return new ValueGridUtilities.Location(Width, Height); }
    }

    /// <summary>
    /// Able to index the valud grid by location
    /// </summary>
    /// <param name="a_Index">Index of value</param>
    /// <returns>Copy of value at index</returns>
    public int this[ValueGridUtilities.Location a_Index]
    {
        get => data[a_Index.X, a_Index.Y];
        set => data[a_Index.X, a_Index.Y] = value; 
    }
}

