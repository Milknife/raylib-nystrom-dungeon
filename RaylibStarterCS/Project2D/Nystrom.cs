﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;

using ValueGridUtilities;

// Hack for Smirnov
using Project2D;

namespace Dungeon.Nystrom
{
    /// <summary>
    /// Nystrom dungeon generation techique implemented as per:
    /// http://journal.stuffwithstuff.com/2014/12/21/rooms-and-mazes/
    /// 
    /// A Minimum of four tile types are required for implementation.
    /// Additionally, all rooms and corridors require region taging during creation
    /// to ensure the spanning dungeon graph is fully connected, this is stored in a
    /// second ValueGrid and discarded upon completion.
    /// 
    /// Implementation:
    ///     1: Fill a space with rooms (Brute Force), adding a unique region to a list per room
    ///     2: Fill the gaps with corridors, adding a unique region to a list per new corridor
    ///     3: Tag valid door/connector positions per room
    ///     4: Starting at a random room:
    ///         a: Open a random door/connector to the region
    ///         b: Flood fill adjoining space with region data
    ///         c: Remove closed doors/connector that would connect the now merged space.
    ///         d: If there are connectors remaining, go to 'a:'
    ///     5: Remove dead ends by filling in corridors that only have one region neighbour
    /// 
    /// </summary>
    public class Generator
    {
        /// <summary>
        /// Required minimal number of tile variations for Nystrom generation
        /// </summary>
        public enum Tile
        {
            None,       // You get nothing! You lose! Good day Sir!
            Wall,       // Fill & Solid
            Room,       // Rectangular type of region
            Corridor,   // The connecting tiles type
            Connector,  // The connector / Door tile
            Count = 4   // Quick reference for Tile type count
        }

        /// <summary>
        /// Read only access to the final generated tile grid formed of Tile Types
        /// </summary>
        public ValueGrid TileGrid { get; private set; } = new ValueGrid(0, 0);

        /// <summary>
        /// Read only access to the initial region grid where values are unique per room
        /// </summary>
        public ValueGrid RegionGrid { get; private set; } = new ValueGrid(0, 0);

        /// <summary>
        /// The current bounds in which to generate a dungeon
        /// </summary>
        private Location bounds = new Location(43, 24);
        /// <summary>
        /// The current bounds in which to generate the next dungeon. Sanity checked
        /// Ensures enough space for at least a 3x3 room and one corridor.
        /// </summary>
        public Location Bounds
        {
            get => bounds;
            set => bounds = new Location(Math.Max(value.X, SmallestRoomExtent + 2), Math.Max(value.Y, SmallestRoomExtent + 2));
        }

        /// <summary>
        /// Smallest possible room size is 3x3
        /// </summary>
        private const int SmallestRoomExtent = 3;

        /// <summary>
        /// Default minimum room size is also the smallest possible
        /// </summary>
        private int minimumRoomExtent = SmallestRoomExtent;
        /// <summary>
        /// Minimum room size, sanity checked
        /// </summary>
        public int MinimumRoomExtent
        {
            get => minimumRoomExtent;
            set => minimumRoomExtent = Math.Min(Math.Max(value, SmallestRoomExtent), maximumRoomExtent - 2);
        }

        /// <summary>
        /// Maximum size of the default room
        /// </summary>
        private int maximumRoomExtent = 9;
        /// <summary>
        /// Minimum room size, sanity checked 
        /// </summary>
        public int MaximumRoomExtent
        {
            get => maximumRoomExtent;
            set => maximumRoomExtent = Math.Max(value, minimumRoomExtent + 2);
        }

        /// <summary>
        /// Ratio of rooms with bonus girth
        /// </summary>
        private float bonusExtentRatio = 0.1f;
        /// <summary>
        /// Ratio of rooms with bonus girth, clamped 0.0f to 1.0f
        /// </summary>
        public float BonusExtentChance
        {
            get => bonusExtentRatio;
            set => bonusExtentRatio = Math.Max(0.0f, Math.Min(value, 1.0f));
        }

        /// <summary>
        /// Minimum bonus extent on room dimension
        /// </summary>
        private int minimumBonusExtent = 1;
        /// <summary>
        /// Minimum bonus extent, sanity checked
        /// </summary>
        public int MinimumBonusExtent
        {
            get => minimumBonusExtent;
            set => minimumBonusExtent = Math.Max(1, Math.Min(value, maximumBonusExtent - 1));
        }

        /// <summary>
        /// Maximum bonus extent on room dimension
        /// </summary>
        private int maximumBonusExtent = 20;
        /// <summary>
        /// Maximum bonus extent, sanity checked
        /// </summary>
        public int MaximumBonusExtent
        {
            get => maximumBonusExtent;
            set => maximumBonusExtent = Math.Max(value, minimumBonusExtent + 2);
        }

        /// <summary>
        /// Maximum attempts to place rooms before halting.
        /// </summary>
        private int maximumRoomPlacementAttempts = 100;
        /// <summary>
        /// Maximum attempts to place rooms before halting.
        /// Sanity checked because you have to try at least once.
        /// </summary>
        public int MaximumRoomPlacementAttempts
        {
            get => maximumRoomPlacementAttempts;
            set => maximumRoomPlacementAttempts = Math.Max(value, 1);
        }

        /// <summary>
        /// Maximum number of successfully placed rooms.
        /// </summary>
        private int maximumRoomCount = 100;
        /// <summary>
        /// Maximum number of successfully placed rooms.
        /// Sanity checked because you should at least one.
        /// </summary>
        public int MaximumRoomCount
        {
            get => maximumRoomCount;
            set => maximumRoomCount = Math.Max(value, 1);
        }

        /// <summary>
        /// The ratio of connecting wall segments that will become additional connections
        /// </summary>
        private float bonusConnectionChance = 0.02f;
        /// <summary>
        /// The ratio of connecting wall segments that will become bonus connections
        /// Sanity checked 0.0f to 1.0f
        /// </summary>
        public float BonusConnectionChance
        {
            get => bonusConnectionChance;
            set => bonusConnectionChance = Math.Max(0.0f, Math.Min(value, 1.0f));
        }

        /// <summary>
        /// The current total count of individual rooms and unique corridors
        /// </summary>
        public int CurrentRegionCount { get; private set; } = 0;
        /// <summary>
        /// The current total count of unique rooms
        /// </summary>
        public int CurrentRoomCount { get; private set; } = 0;
        /// <summary>
        /// The current total count of unique corridors
        /// </summary>
        public int CurrentCorridorCount { get; private set; } = 0;

        /// <summary>
        /// Connectors are a unique region, here is its current value
        /// </summary>
        public int ConnectorRegionID { get; private set; } = 0;

        /// <summary>
        /// Storage of creation duration in milliseconds
        /// </summary>
        public float CreationDuration { get; private set; } = 0.0f;

        /// <summary>
        /// The seed for the random number generator for the next dungeon generated
        /// </summary>
        public int NextRNGSeed { get; set; } = 0;
        /// <summary>
        /// The seed for the random number generator for the next dungeon generated
        /// </summary>
        public int CurrentRNGSeed { get; private set; } = 0;
        /// <summary>
        /// The Random number generator object for this dungeon
        /// </summary>
        private Random rng;

        /// <summary>
        /// Internal safe bounds to place rooms and corridors
        /// </summary>
        private Location safePlacementBounds;

        /// <summary>
        /// Stopwatch used for calculating how long the dungeon creation took
        /// </summary>
        private readonly Stopwatch stopWatch = new Stopwatch();

        /// <summary>
        /// Create a dungeon using the current set values
        /// and Nystrom's technique
        /// </summary>
        public void Generate()
        {
            // Start the clock!
            stopWatch.Restart();

            // Set up the memory and objects
            Initialize();
            // Now fill with rooms
            FillWithRooms( true ); // True to use Smirnov Table
            // Then fill with corridors
            FillWithCorridors();
            // Then open up connections
            FillWithDoors();
            // Remove the dead ends only if rooms exist
            if (CurrentRoomCount > 0)
            {
                FillDeadEnds();
            }
            // Remove wall fill
            RemoveSuperfluousWalls();
            
            // Report in on how long it all took
            stopWatch.Stop();
            // and store it in seconds
            CreationDuration = stopWatch.ElapsedMilliseconds * 0.001f;
        }

        /// <summary>
        /// Set up our dungeon wide values and memory expectation
        /// </summary>
        private void Initialize()
        {
            // Prepare both value grids
            TileGrid = new ValueGrid(Bounds);
            RegionGrid = new ValueGrid(Bounds);

            // Set the grid to fill with 'wall'
            for (int X = 0; X < Bounds.X; ++X)
            {
                for (int Y = 0; Y < Bounds.Y; ++Y)
                {
                    TileGrid[X, Y] = (int)Tile.Wall;
                    RegionGrid[X, Y] = (int)Tile.None;
                }
            }

            // Sanitise the bounds for odd feature placement
            safePlacementBounds = Bounds;
            if (safePlacementBounds.X % 2 == 0) { safePlacementBounds.X -= 1; }
            if (safePlacementBounds.Y % 2 == 0) { safePlacementBounds.Y -= 1; }

            // Region count during allocation
            CurrentRegionCount = 0;
            CurrentRoomCount = 0;
            CurrentCorridorCount = 0;

            // Initialize the RNG
            // If no seed set...
            if (NextRNGSeed == 0)
            {
                // Use the time.
                NextRNGSeed = (int)DateTime.Now.ToFileTime();
                CurrentRNGSeed = NextRNGSeed;
            }
            // Create the new RNG...
            rng = new Random(NextRNGSeed);
            // and reset the seed
            NextRNGSeed = 0;
        }

        /// <summary>
        /// Fill the value grid with rectangular spaces
        /// Ensuring no overlap, and valid positioning.
        /// Try this as long as there are attempts remaining,
        ///   and we have not exceeded out maximum room count
        /// </summary>
        private void FillWithRooms(bool smirnov = false)
        {
            /* Save a little on garbage collection by not reallocating these each loop */
            // Room position
            Location roomOrigin = new Location();
            // Room dimensions
            Location roomExtents = new Location();
            // Furthest corner
            Location roomFurthest = new Location();

            List<SmirnovPair> smirnovPairs = new List<SmirnovPair>();
            smirnovPairs.Add(new SmirnovPair(new ValueGrid(5, 3), 5));
            smirnovPairs.Add(new SmirnovPair(new ValueGrid(3, 5), 5));
            smirnovPairs.Add(new SmirnovPair(new ValueGrid(5, 5), 2.5f));
            smirnovPairs.Add(new SmirnovPair(new ValueGrid(3, 3), 1));
            smirnovPairs.Add(new SmirnovPair(new ValueGrid(3, 21), 100));

            SmirnovTransform st = new SmirnovTransform(smirnovPairs);

            // For as many attempts as we can get away with, or until we reach our limit...
            for (int attempt = 0;
                 ((attempt < maximumRoomPlacementAttempts) && (CurrentRoomCount < MaximumRoomCount));
                 attempt++)
            {
                // Pick the start grid for this room
                roomOrigin.X = rng.Next(safePlacementBounds.X);
                roomOrigin.Y = rng.Next(safePlacementBounds.Y);

                // Calculate the end grid for this room
                roomExtents.X = rng.Next(minimumRoomExtent, maximumRoomExtent);
                roomExtents.Y = rng.Next(minimumRoomExtent, maximumRoomExtent);

                // Apply bonus dimensions if applicable
                // Chance is 1 in 'BonusExtentChance' per dimension per room
                if (rng.NextDouble() < BonusExtentChance) { roomExtents.X += rng.Next(minimumBonusExtent, maximumBonusExtent); }
                if (rng.NextDouble() < BonusExtentChance) { roomExtents.Y += rng.Next(minimumBonusExtent, maximumBonusExtent); }

                // Upgrade?
                if (smirnov)
                {
                    ValueGrid Extents = st.Sample();
                    roomExtents.X = Extents.Width;
                    roomExtents.Y = Extents.Height;
                }

                // Ensure it is an odd grid reference
                if (roomOrigin.X % 2 == 0) { roomOrigin.X--; }
                if (roomOrigin.Y % 2 == 0) { roomOrigin.Y--; }

                // Ensure it is an odd grid reference
                if (roomExtents.X % 2 == 0) { roomExtents.X++; }
                if (roomExtents.Y % 2 == 0) { roomExtents.Y++; }

                // Clamp to bounds in case of rediculousness
                roomExtents.X = Math.Min(roomExtents.X, safePlacementBounds.X - 1);
                roomExtents.Y = Math.Min(roomExtents.Y, safePlacementBounds.Y - 1);

                // Sum up the opposite corner to roomOrigin
                roomFurthest.X = roomOrigin.X + roomExtents.X;
                roomFurthest.Y = roomOrigin.Y + roomExtents.Y;

                // Bounds check roomOrigin , and roomOrigin + roomExtents
                if (!Location.BoundsCheck(roomOrigin, Location.Origin, safePlacementBounds) ||
                    !Location.BoundsCheck(roomFurthest, Location.Origin, safePlacementBounds))
                {
                    // Out of bounds, reject, and return to the top of the loop
                    continue;
                }

                // Check all tiles between RoomMinimum and RoomMaximum
                bool clear = true;
                for (int X = roomOrigin.X; X < roomFurthest.X; ++X)
                {
                    for (int Y = roomOrigin.Y; Y < roomFurthest.Y; ++Y)
                    {
                        // Tile in the way...
                        if (TileGrid[X, Y] == (int)Tile.Room)
                        {
                            // Leave
                            clear = false;
                            break;
                        }
                    }
                    // Leave
                    if (!clear) { break; }
                }

                // If clear, set as room.
                if (clear)
                {
                    // Increment the room...
                    CurrentRoomCount++;
                    // and region counters
                    CurrentRegionCount++;

                    for (int X = roomOrigin.X; X < roomFurthest.X; ++X)
                    {
                        for (int Y = roomOrigin.Y; Y < roomFurthest.Y; ++Y)
                        {
                            TileGrid[X, Y] = (int)Tile.Room;
                            RegionGrid[X, Y] = CurrentRegionCount;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Fill the gaps with corridors, adding a unique region per new corridor.
        /// Starting at a valid wall, snake though the walls left by the rooms where valid.
        /// Randomly turning left and right while seaching.
        /// </summary>
        private void FillWithCorridors()
        {
            // We are only done seaching for space to place corridors, when the dungeon is full.
            bool searching = true;

            // Start one in, no point seatching the edge
            Location searchIndex = new Location(1, 1);
            // Adjacency list used for tile neighbour tracking
            List<Location> adjacency = new List<Location>();
            // Corridor stack used to process potential corridor branching
            Stack<Location> corridorStack = new Stack<Location>();

            // While there is still search space...
            while (searching)
            {
                // Assume we are done until proven otherwise.
                searching = false;

                // A new corridor
                CurrentCorridorCount++;
                // and a new region
                CurrentRegionCount++;

                // Find next space
                for (int X = 1; X < safePlacementBounds.X; X += 2)
                {
                    for (int Y = 1; Y < safePlacementBounds.Y; Y += 2)
                    {
                        if (TileGrid[X, Y] == (int)Tile.Wall)
                        {
                            searchIndex.X = X;
                            searchIndex.Y = Y;
                            searching = true;
                            break;
                        }
                    }
                    // We have found and stored the location to the start of our next corridor
                    if (searching) { break; }
                }
                // If we are done searching, it is because the grid is full - Hooray!
                if (!searching) { break; }

                // Stack of valid corridor branches
                corridorStack.Clear();

                // Add the most recently found, valid grid
                corridorStack.Push(searchIndex);

                // While there are connected grid locations to path corridors through...
                while (corridorStack.Count > 0)
                {
                    // Clear the adjacent list
                    adjacency.Clear();
                    // Pop the current location and check for adjacency
                    Location current = corridorStack.Pop();

                    // Check all directions and list valid directions
                    if ((current.NextX().NextX().X < safePlacementBounds.X) &&
                        (TileGrid[current.NextX().NextX()] == (int)Tile.Wall))
                    {
                        adjacency.Add(current.NextX().NextX());
                    }
                    if ((current.PrevX().PrevX().X > 0) &&
                        (TileGrid[current.PrevX().PrevX()] == (int)Tile.Wall))
                    {
                        adjacency.Add(current.PrevX().PrevX());
                    }
                    if ((current.NextY().NextY().Y < safePlacementBounds.Y) &&
                        (TileGrid[current.NextY().NextY()] == (int)Tile.Wall))
                    {
                        adjacency.Add(current.NextY().NextY());
                    }
                    if ((current.PrevY().PrevY().Y > 0) &&
                        (TileGrid[current.PrevY().PrevY()] == (int)Tile.Wall))
                    {
                        adjacency.Add(current.PrevY().PrevY());
                    }

                    // Are we at a dead end?....
                    if (adjacency.Count == 0)
                    {
                        TileGrid[current] = (int)Tile.Corridor;
                        RegionGrid[current] = CurrentRegionCount;
                        continue;
                    }
                    // ...or return here (location) to branch later.
                    else if (adjacency.Count > 1)
                    {
                        corridorStack.Push(current);
                    }

                    // Pick a random, valid direction
                    Location next = adjacency[rng.Next(adjacency.Count)];

                    // Path to, and in between
                    TileGrid[current] = (int)Tile.Corridor;
                    TileGrid[(current.X + next.X) / 2, (current.Y + next.Y) / 2] = (int)Tile.Corridor;
                    TileGrid[next] = (int)Tile.Corridor;

                    // Set the region
                    RegionGrid[current] = CurrentRegionCount;
                    RegionGrid[(current.X + next.X) / 2, (current.Y + next.Y) / 2] = CurrentRegionCount;
                    RegionGrid[next] = CurrentRegionCount;

                    // Continue down this corridor
                    corridorStack.Push(next);
                }
            }
        }

        /// <summary>
        /// Tag valid door/connector positions per region     
        /// Starting at a random region
        ///     a: Opens a random door/connector to the region
        ///     b: Flood fill adjoining space with region data
        ///     c: Remove closed doors/connector that would connect the now merged space.
        ///     d: If there are connectors remaining, go to 'a:'
        /// </summary>
        private void FillWithDoors()
        {
            //List of connec for storage
            List<Location> connectors = new List<Location>();

            // Location storage for our tiny sample kernal
            Location currentWall = new Location();
            Location neighbourA = new Location();
            Location neighbourB = new Location();

            // Within the space of all valid rooms 
            // tag all possible door tiles
            for (int X = 1; X < safePlacementBounds.X - 1; ++X)
            {
                for (int Y = 1; Y < safePlacementBounds.Y - 1; ++Y)
                {
                    // If it's not a wall, it can't be a door.
                    if (TileGrid[X, Y] != (int)Tile.Wall) { continue; }

                    // Set the local centre sample
                    currentWall.X = X;
                    currentWall.Y = Y;

                    // Set up a short lambda
                    object checker()
                    {
                        // If either side is a wall
                        if ((RegionGrid[neighbourA] == 0) || (RegionGrid[neighbourB] == 0))
                        {
                            //This cannot be a connector in this direction
                        }
                        // If either side is a room and...
                        else if ((RegionGrid[neighbourA] > 0 && RegionGrid[neighbourA] <= CurrentRoomCount) ||
                                 (RegionGrid[neighbourB] > 0 && RegionGrid[neighbourB] <= CurrentRoomCount))
                        {
                            // If the connector is not already listed...
                            if (!connectors.Contains(currentWall))
                            {
                                // Add to the collection
                                connectors.Add(currentWall);
                            }
                        }
                        return null;
                    }

                    // Horizontal Check
                    neighbourA = currentWall.NextX();
                    neighbourB = currentWall.PrevX();
                    checker();

                    // Vertical check
                    neighbourA = currentWall.NextY();
                    neighbourB = currentWall.PrevY();
                    checker();
                }
            }

            // Now we have a list of potential connections,
            // pick one at random, set a merged region id,
            // a unique id for connectors.
            int connectorIndex = rng.Next(connectors.Count);
            int mergedRegionID = CurrentRegionCount + 1;
            ConnectorRegionID = mergedRegionID + 1;

            // The first one is always valid.
            Queue<Location> FloodFillQueue = new Queue<Location>();

            // This is for the first found, but sometimes, there are none (ie. No rooms either)
            if (connectors.Count > 0)
            {
                // Enque the first as the start of the flood fill
                FloodFillQueue.Enqueue(connectors[connectorIndex]);

                // Tag it as a connector in both tile
                TileGrid[connectors[connectorIndex]] = (int)Tile.Connector;
                // and region
                RegionGrid[connectors[connectorIndex]] = ConnectorRegionID;

                // Remove from master connector list
                connectors.RemoveAt(connectorIndex);
            }

            // Grid tracks all regions as the merge forming a 
            // spanning graph. We want to preserve the room
            // region data, however, so we take a copy.
            ValueGrid MergedGrid = new ValueGrid(RegionGrid);
            // Save some allocation.
            Location currentTile;

            // While there are still connectors to processs
            while (connectors.Count > 0)
            {
                // If the flood fill queue is still processing
                while (FloodFillQueue.Count > 0)
                {
                    // flood fill set to zero through the hole
                    currentTile = FloodFillQueue.Dequeue();
                    // Setting the mergedID is the flood fill color.
                    MergedGrid[currentTile] = mergedRegionID;

                    // Now test the four adjacent tiles are not a wall or already merged.
                    // +ve X
                    if ((MergedGrid[currentTile.NextX()] != 0) &&
                        (MergedGrid[currentTile.NextX()] != mergedRegionID))
                    {
                        if (!FloodFillQueue.Contains(currentTile.NextX()))
                        {
                            FloodFillQueue.Enqueue(currentTile.NextX());
                        }
                    }
                    // +ve Y
                    if ((MergedGrid[currentTile.NextY()] != 0) &&
                        (MergedGrid[currentTile.NextY()] != mergedRegionID))
                    {
                        if (!FloodFillQueue.Contains(currentTile.NextY()))
                        {
                            FloodFillQueue.Enqueue(currentTile.NextY());
                        }
                    }
                    // -ve X
                    if ((MergedGrid[currentTile.PrevX()] != 0) &&
                        (MergedGrid[currentTile.PrevX()] != mergedRegionID))
                    {
                        if (!FloodFillQueue.Contains(currentTile.PrevX()))
                        {
                            FloodFillQueue.Enqueue(currentTile.PrevX());
                        }
                    }

                    // -ve Y
                    if ((MergedGrid[currentTile.PrevY()] != 0) &&
                        (MergedGrid[currentTile.PrevY()] != mergedRegionID))
                    {
                        if (!FloodFillQueue.Contains(currentTile.PrevY()))
                        {
                            FloodFillQueue.Enqueue(currentTile.PrevY());
                        }
                    }
                }

                // Any connectors still adjacent to the merged region must be culled,
                // but have a chance of being opened anyway
                for (int i = connectors.Count - 1; i >= 0; --i)
                {
                    currentTile = connectors[i];

                    // Horizontally - Does the current tile connect the merged region to itself? 
                    if ((MergedGrid[currentTile.NextX()] == mergedRegionID) &&
                        (MergedGrid[currentTile.PrevX()] == mergedRegionID))
                    {
                        // Possible to open anyway but...
                        if (rng.NextDouble() < bonusConnectionChance)
                        {
                            TileGrid[currentTile] = (int)Tile.Connector;
                            RegionGrid[currentTile] = ConnectorRegionID;
                            MergedGrid[currentTile] = mergedRegionID;
                        }
                        // Remove regardless!
                        connectors.Remove(currentTile);
                    }
                    // Vertically - Does the current tile connect the merged region to itself?
                    else if ((MergedGrid[currentTile.NextY()] == (int)mergedRegionID) &&
                             (MergedGrid[currentTile.PrevY()] == (int)mergedRegionID))
                    {
                        // Possible to open anyway but...
                        if (rng.NextDouble() < bonusConnectionChance)
                        {
                            TileGrid[currentTile] = (int)Tile.Connector;
                            RegionGrid[currentTile] = ConnectorRegionID;
                            MergedGrid[currentTile] = mergedRegionID;
                        }
                        // Remove regardless!
                        connectors.Remove(currentTile);
                    }
                }

                // If still connectors in the list find a connector that touches
                // the merged region on one side and open it.
                if (connectors.Count > 0)
                {
                    int randomIndex = 0;
                    bool validMergedRegionConnector = false;

                    // As this is a spanning tree, it will never be disjointed
                    while (!validMergedRegionConnector)
                    {
                        // just need to find the next available connector
                        randomIndex = rng.Next(connectors.Count);
                        validMergedRegionConnector =
                            (MergedGrid[connectors[randomIndex].NextX()] == mergedRegionID) ||
                            (MergedGrid[connectors[randomIndex].PrevX()] == mergedRegionID) ||
                            (MergedGrid[connectors[randomIndex].NextY()] == mergedRegionID) ||
                            (MergedGrid[connectors[randomIndex].PrevY()] == mergedRegionID);
                    }

                    // Enque the new connector as the start of the flood fill
                    FloodFillQueue.Enqueue(connectors[randomIndex]);
                    // Tag it as a connector in both tile
                    TileGrid[connectors[randomIndex]] = (int)Tile.Connector;
                    // and region
                    RegionGrid[connectors[randomIndex]] = ConnectorRegionID;
                    // Remove from master connector list
                    connectors.RemoveAt(randomIndex);
                }
            }
        }

        /// <summary>
        /// Unwinds corridors that terminate as deadends.
        /// </summary>
        private void FillDeadEnds()
        {
            // Storage for tracking deadends
            List<Location> deadEnds = new List<Location>();
            // Temp. location as a lot of indexing takes place
            Location t_Location = new Location();
            // Our loop control flag
            bool removingDeadEnds = true;

            // While more deadends to unwind
            while (removingDeadEnds)
            {
                // Clear the list
                deadEnds.Clear();

                // Iterate and check...
                for (int X = 0; X < Bounds.X; ++X)
                {
                    for (int Y = 0; Y < Bounds.Y; ++Y)
                    {
                        t_Location.X = X;
                        t_Location.Y = Y;

                        // If its region is above the room count, it is a corridor
                        if (RegionGrid[t_Location] > CurrentRoomCount)
                        {
                            int neighbourCount = 0;
                            // Count adjoining walls
                            neighbourCount += (RegionGrid[t_Location.NextX()] == 0) ? 1 : 0;
                            neighbourCount += (RegionGrid[t_Location.PrevX()] == 0) ? 1 : 0;
                            neighbourCount += (RegionGrid[t_Location.NextY()] == 0) ? 1 : 0;
                            neighbourCount += (RegionGrid[t_Location.PrevY()] == 0) ? 1 : 0;

                            // if it neightbours three walls
                            if (neighbourCount >= 3)
                            {
                                // flag for deletion
                                deadEnds.Add(t_Location);
                            }
                        }
                    }
                }

                // If no deadends found,
                if (deadEnds.Count == 0)
                {
                    // we are done.
                    removingDeadEnds = false;
                }
                else
                {
                    // Remove the dead ends for the list and return to the top to
                    // see if we have created more.
                    for (int index = 0; index < deadEnds.Count; ++index)
                    {
                        TileGrid[deadEnds[index]] = (int)Tile.Wall;
                        RegionGrid[deadEnds[index]] = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Reduces all walls to thicknes of 'one' leaving voids where walls are removed
        /// </summary>
        private void RemoveSuperfluousWalls()
        { 
            // Keeping track of walls to remove
            List<Location> locationsToClear = new List<Location>();
            Location currentLocation = new Location();

            // Check every tile...
            for (int X = 0; X < TileGrid.Width; ++X)
            {
                for (int Y = 0; Y < TileGrid.Height; ++Y)
                {
                    // Stoe the current location for testing
                    currentLocation.X = X;
                    currentLocation.Y = Y;

                    // If the current location is a wall,
                    // Bounds check and test all eight neighbours as walls.
                    if (TileGrid[currentLocation] == (int)(Tile.Wall))
                    {
                        if (Location.BoundsCheck(currentLocation.NextX(), Location.Origin, Bounds))
                        {
                            if (TileGrid[currentLocation.NextX()] != (int)Tile.Wall)
                            {
                                continue;
                            }
                        }
                        if (Location.BoundsCheck(currentLocation.PrevX(), Location.Origin, Bounds))
                        {
                            if (TileGrid[currentLocation.PrevX()] != (int)Tile.Wall)
                            {
                                continue;
                            }
                        }
                        if (Location.BoundsCheck(currentLocation.NextY(), Location.Origin, Bounds))
                        {
                            if (TileGrid[currentLocation.NextY()] != (int)Tile.Wall)
                            {
                                continue;
                            }
                        }
                        if (Location.BoundsCheck(currentLocation.PrevY(), Location.Origin, Bounds))
                        {
                            if (TileGrid[currentLocation.PrevY()] != (int)Tile.Wall)
                            {
                                continue;
                            }
                        }
                        if (Location.BoundsCheck(currentLocation.NextX().NextY(), Location.Origin, Bounds))
                        {
                            if (TileGrid[currentLocation.NextX().NextY()] != (int)Tile.Wall)
                            {
                                continue;
                            }
                        }
                        if (Location.BoundsCheck(currentLocation.PrevX().PrevY(), Location.Origin, Bounds))
                        {
                            if (TileGrid[currentLocation.PrevX().PrevY()] != (int)Tile.Wall)
                            {
                                continue;
                            }
                        }
                        if (Location.BoundsCheck(currentLocation.NextY().PrevX(), Location.Origin, Bounds))
                        {
                            if (TileGrid[currentLocation.NextY().PrevX()] != (int)Tile.Wall)
                            {
                                continue;
                            }
                        }
                        if (Location.BoundsCheck(currentLocation.PrevY().NextX(), Location.Origin, Bounds))
                        {
                            if (TileGrid[currentLocation.PrevY().NextX()] != (int)Tile.Wall)
                            {
                                continue;
                            }
                        }

                        // This far, we haven't exited, so list for changing to a void.
                        locationsToClear.Add(currentLocation);
                    }
                }
            }

            // Set all found walls to none.
            for (int index = 0; index < locationsToClear.Count; ++index)
            {
                TileGrid[locationsToClear[index]] = (int)(Tile.None);
            }
        }

        /// <summary>
        /// Save this Nystrom generated dungeon to file
        /// </summary>
        /// <param name="a_fileName">Name of file to save to</param>
        /// <param name="a_writer">Optional writer object, with file handle open</param>
        public void Save(String a_fileName, BinaryWriter a_writer = null)
        {
            // Establish a writer
            BinaryWriter writer;
            writer = a_writer ?? new BinaryWriter(File.Open(a_fileName, FileMode.Create));

            // Write out the ValueGrids
            TileGrid.Save(a_fileName, writer);
            RegionGrid.Save(a_fileName, writer);

            // Write out the member data
            writer.Write(bounds.X);
            writer.Write(bounds.Y);
            writer.Write(minimumRoomExtent);
            writer.Write(maximumRoomExtent);
            writer.Write(bonusExtentRatio);
            writer.Write(minimumBonusExtent);
            writer.Write(maximumBonusExtent);
            writer.Write(maximumRoomPlacementAttempts);
            writer.Write(maximumRoomCount);
            writer.Write(bonusConnectionChance);
            writer.Write(CurrentRegionCount);
            writer.Write(CurrentRoomCount);
            writer.Write(CurrentCorridorCount);
            writer.Write(ConnectorRegionID);
            writer.Write(CreationDuration);
            writer.Write(CurrentRNGSeed);
            writer.Write(safePlacementBounds.X);
            writer.Write(safePlacementBounds.Y);

            // Close the file handle
            writer.Close();
        }

        /// <summary>
        /// Load this Nystrom generated dungeon from file
        /// </summary>
        /// <param name="a_fileName">Name of file to read from</param>
        /// <param name="a_reader">Optional reader object, with file handle open</param>
        public void Load(String a_fileName, BinaryReader a_reader = null)
        {
            // Establish a reader
            BinaryReader reader;
            reader = a_reader ?? new BinaryReader(File.Open(a_fileName, FileMode.Open));

            // Read out the ValueGrids
            TileGrid.Load(a_fileName, reader);
            RegionGrid.Load(a_fileName, reader);

            // Read the member data
            bounds.X = reader.ReadInt32();
            bounds.Y = reader.ReadInt32();
            minimumRoomExtent = reader.ReadInt32();
            maximumRoomExtent = reader.ReadInt32();
            bonusExtentRatio = reader.ReadSingle();
            minimumBonusExtent = reader.ReadInt32();
            maximumBonusExtent = reader.ReadInt32();
            maximumRoomPlacementAttempts = reader.ReadInt32();
            maximumRoomCount = reader.ReadInt32();
            bonusConnectionChance = reader.ReadSingle();
            CurrentRegionCount = reader.ReadInt32();
            CurrentRoomCount = reader.ReadInt32();
            CurrentCorridorCount = reader.ReadInt32();
            ConnectorRegionID = reader.ReadInt32();
            CreationDuration = reader.ReadSingle();
            CurrentRNGSeed = reader.ReadInt32();
            safePlacementBounds.X = reader.ReadInt32();
            safePlacementBounds.Y = reader.ReadInt32();

            // Close the file handle
            reader.Close();
        }
    }

}
