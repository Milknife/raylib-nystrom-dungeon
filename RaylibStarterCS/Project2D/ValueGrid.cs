﻿using System;
using System.IO;

/// <summary>
/// The lowests form of data representation for a two dimentional grid
/// before bitwise optimisation are needed.
/// </summary>
/// 
public partial class ValueGrid
{
    /// <summary>
    /// Readonly horizontal tile count
    /// </summary>
    public int Width { get; private set; } = 0;
    /// <summary>
    /// Readonly vertical tile count
    /// </summary>
    public int Height { get; private set; } = 0;

    /// <summary>
    /// Rectangular grid of data 
    /// </summary>
    private int[,] data = null;

    /// <summary>
    /// Prepare the rectangualr array of values
    /// </summary>
    /// <param name="a_width">Number of vertical values</param>
    /// <param name="a_height">Number of horizontal values</param>
    public ValueGrid(uint a_width, uint a_height)
    {
        Width = (int)a_width;
        Height = (int)a_height;
        data = new int[Width, Height];
    }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="a_Width">Number of vertical values</param>
    /// <param name="a_Height">Number of horizontal values</param>
    public ValueGrid(ValueGrid a_valueGrid)
    {
        Width = a_valueGrid.Width;
        Height = a_valueGrid.Height;
        data = new int[Width, Height];

        // Deep copy of data
        for (int X = 0; X < Width; ++X)
        {
            for (int Y = 0; Y < Height; ++Y)
            {
                data[X, Y] = a_valueGrid.data[X, Y];
            }
        }
    }


    /// <summary>
    /// Grid data property
    /// </summary>
    /// <param name="a_X">Grid's column index</param>
    /// <param name="a_Y">Grid's row index</param>
    /// <returns>Reference to value at index</returns>
    public int this[int a_X, int a_Y]
    {
        get { return data[a_X, a_Y]; }
        set { data[a_X, a_Y] = value; }
    }
    
    /// <summary>
    /// Save this ValueGrid to file
    /// </summary>
    /// <param name="a_fileName">Name of file to save to</param>
    /// <param name="a_writer">Optional writer object, with file handle open</param>
    public void Save(String a_fileName, BinaryWriter a_writer = null, bool a_closeAfter = false)
    {
        // Establish a writer
        BinaryWriter writer;
        writer = a_writer ?? new BinaryWriter(File.Open(a_fileName, FileMode.Open));

        // Write the dimensions first
        writer.Write(Width);
        writer.Write(Height);

        // Write the data 
        for (int X = 0; X < Width; X++)
        {
            for (int Y = 0; Y < Height; Y++)
            {
                writer.Write(data[X, Y]);
            }
        }
        // Close the file handle
        if (a_closeAfter)
        {
            writer.Close();
        }
    }

    /// <summary>
    /// Load a ValueGrid of data from file
    /// </summary>
    /// <param name="a_fileName">Name of file to read from</param>
    /// <param name="a_reader">Optional reader object, with file handle open</param>
    public void Load(String a_fileName, BinaryReader a_reader = null, bool a_closeAfter = false)
    {
        // Establish a reader
        BinaryReader reader;
        reader = a_reader ?? new BinaryReader(File.Open(a_fileName, FileMode.Open));
        // Read the dimensions first
        Width = reader.ReadInt32();
        Height = reader.ReadInt32();

        // Initialize the array
        data = new int[Width, Height];

        // Read the data 
        for (uint X = 0; X < Width; X++)
        {
            for (uint Y = 0; Y < Height; Y++)
            {
                data[X, Y] = reader.ReadInt32();
            }
        }
        // Close the file handle
        if (a_closeAfter)
        {
            reader.Close();
        }
    }
}

