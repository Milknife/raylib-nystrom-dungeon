﻿
using System.Linq;

using System;
using Raylib;
using static Raylib.Raylib;
using Dungeon;

namespace Dungeon.Nystrom
{
    /// <summary>
    /// Renders a value grid to an unknown number of colours
    /// </summary>
    public class Renderer
    {
        /// <summary>
        /// Default count for 1920 with 32px tiles
        /// </summary>
        public int HorizontalTileCount { get; private set; } = 60;
        /// <summary>
        /// Default count for 1080 with 32px tiles
        /// </summary>
        public int VerticalTileCount { get; private set; } = 33;
        /// <summary>
        /// Default Tile Resolution 32x32
        /// </summary>
        public int TileResolutionX { get; private set; } = 32;
        /// <summary>
        /// Default Tile Resolution 32x32
        /// </summary>
        public int TileResolutionY { get; private set; } = 32;

        /// <summary>
        /// Scale a tile needs to shrink or grow to fill its pixel size on screen
        /// </summary>
        private float scaleXY;
        /// <summary>
        /// Size of a tile in pixels after applying scale
        /// </summary>
        public float SizeXY { get; private set; }

        /// <summary>
        /// Four base values of all value grids
        /// </summary>
        private readonly int tileTypeCount = (int)Nystrom.Generator.Tile.Count;
        /// <summary>
        /// Storage for up to 5 textures
        /// </summary>
        public readonly Texture2D[] TileTextures;

        public Color WallTint { get; set; } = Color.GRAY;
        public Color CorridorTint { get; set; } = Color.LIGHTGRAY;
        public Color ConnectorTint { get; set; } = Color.BROWN;


        public Renderer()
        {
            // Set storage
            TileTextures = new Texture2D[tileTypeCount + 1];

            // Load defaults
            //TileTextures[0] = 'Intentionally left blank'
            TileTextures[(int)Nystrom.Generator.Tile.Wall] = LoadTextureFromImage(LoadImage("../Images/Wall.png"));
            TileTextures[(int)Nystrom.Generator.Tile.Room] = LoadTextureFromImage(LoadImage("../Images/Floor.png"));
            TileTextures[(int)Nystrom.Generator.Tile.Corridor] = LoadTextureFromImage(LoadImage("../Images/Corridor.png"));
            TileTextures[(int)Nystrom.Generator.Tile.Connector] = LoadTextureFromImage(LoadImage("../Images/Floor.png"));

            // Average tile width (Incase source image is modded)
            TileResolutionX = 0;
            for (int index = 1; index < TileTextures.Length; ++index)
            {
                TileResolutionX += TileTextures[index].width;
            }
            TileResolutionX = TileResolutionX / tileTypeCount;

            // Average tile height (Incase source image is modded)
            TileResolutionY = 0;
            for (int index = 1; index < TileTextures.Length; ++index)
            {
                TileResolutionY += TileTextures[index].height;
            }
            TileResolutionY = TileResolutionY / tileTypeCount;
        }

        /// <summary>
        /// Draw the four primative tile types, with Region grid values
        /// </summary>
        /// <param name="a_valueGrid"></param>
        public void Draw(Nystrom.Generator a_nystrom, float a_saturation = 1.0f)
        {
            int screenWidth = GetScreenWidth();
            int screenHeight = GetScreenHeight();

            // Calculate unscaled tile count
            HorizontalTileCount = screenWidth / TileResolutionX;
            VerticalTileCount = screenHeight / TileResolutionY;

            // Calculate Ratios on each axis
            float ratioOnX = HorizontalTileCount / (float)a_nystrom.Bounds.X;
            float ratioOnY = VerticalTileCount / (float)a_nystrom.Bounds.Y;

            // Work out the final screen space pixel size of each tile
            scaleXY = Math.Min(ratioOnX, ratioOnY);
            SizeXY = scaleXY * Math.Max(TileResolutionX, TileResolutionY);

            // Hue as a ratio of all regions counted
            float hue = 360.0f / a_nystrom.CurrentRoomCount;

            // Loop through and draw
            Vector2 position = new Vector2(0, 0);
            for (int X = 0; X < a_nystrom.Bounds.X; X++)
            {
                for (int Y = 0; Y < a_nystrom.Bounds.Y; Y++)
                {
                    position.x = SizeXY * X;
                    position.y = SizeXY * Y;

                    // Calculate the texture index and color distribution
                    int regionValue = a_nystrom.RegionGrid[X, Y];
                    // If regionValue not none, or a wall, set HSV
                    Color tintColor = ((regionValue > 1) ? HSVtoColor(hue * regionValue, a_saturation) : WallTint);
                    // Set if it was a corridor though.
                    tintColor = ((regionValue <= a_nystrom.CurrentRoomCount) ? tintColor : CorridorTint);
                    // Set if it was a connector too
                    tintColor = ((regionValue == a_nystrom.ConnectorRegionID) ? ConnectorTint : tintColor);

                    // Derive the type from the region ranges
                    DrawTextureEx(TileTextures[a_nystrom.TileGrid[X, Y]], position, 0.0f, scaleXY, tintColor);
                }
            }
        }

        /// <summary>
        /// Create a rgba color from HSV values
        /// https://www.rapidtables.com/convert/color/hsv-to-rgb.html
        /// https://en.wikipedia.org/wiki/HSL_and_HSV#HSV_to_RGB
        /// </summary>
        /// <param name="a_hue">Hue</param>
        /// <param name="a_saturation">Saturation</param>
        /// <param name="a_value">Value</param>
        /// <returns>Colour struct in RGB form</returns>
        public Color HSVtoColor(float a_hue, float a_saturation = 1.0f, float a_value = 1.0f)
        {
            float primaryChroma = a_value * a_saturation;
            float secondaryChroma = primaryChroma * (1.0f - Math.Abs((a_hue / 60.0f) % 2 - 1.0f));
            float offset = a_value - primaryChroma;

            float rPrime = 0;
            float gPrime = 0;
            float bPrime = 0;

            // Crawl around the six triangles on the hue plane
            // to Find primary and secondary chroma
            if (a_hue >= 0 && a_hue < 60)
            {
                rPrime = primaryChroma;
                gPrime = secondaryChroma;
                bPrime = 0;
            }
            else if (a_hue >= 60 && a_hue < 120)
            {
                rPrime = secondaryChroma;
                gPrime = primaryChroma;
                bPrime = 0;
            }
            else if (a_hue >= 120 && a_hue < 180)
            {
                rPrime = 0;
                gPrime = primaryChroma;
                bPrime = secondaryChroma;
            }
            else if (a_hue >= 180 && a_hue < 240)
            {
                rPrime = 0;
                gPrime = secondaryChroma;
                bPrime = primaryChroma;
            }
            else if (a_hue >= 240 && a_hue < 300)
            {
                rPrime = secondaryChroma;
                gPrime = 0;
                bPrime = primaryChroma;
            }
            else// if (a_hue >= 0 && a_hue < 360)
            {
                rPrime = primaryChroma;
                gPrime = 0;
                bPrime = secondaryChroma;
            }

            // Build the colour from 0.0-1.0f into 0-255 bytes
            return new Color((byte)((rPrime + offset) * 255), (byte)((gPrime + offset) * 255), (byte)((bPrime + offset) * 255), (byte)255);
        }
    }
}