﻿using System;
using Raylib;
using static Raylib.Raylib;

/// <summary>
/// Renders a value grid to an expected tileset of size 4
/// </summary>
public class ValueGridRenderer
{
    /// <summary>
    /// Default count for 1920 with 32px tiles
    /// </summary>
    public int HorizontalTileCount { get; private set; } = 60;
    /// <summary>
    /// Default count for 1080 with 32px tiles
    /// </summary>
    public int VerticalTileCount { get; private set; } = 33;
    /// <summary>
    /// Default Tile Resolution 32x32
    /// </summary>
    public int TileResolutionX { get; private set; } = 32;
    /// <summary>
    /// Default Tile Resolution 32x32
    /// </summary>
    public int TileResolutionY { get; private set; } = 32;

    /// <summary>
    /// Scale a tile needs to shrink or grow to fill its pixel size on screen
    /// </summary>
    private float scaleXY;
    /// <summary>
    /// Size of a tile in pixels after applying scale
    /// </summary>
    private float sizeXY;

    /// <summary>
    /// Four base values of all value grids
    /// </summary>
    private readonly int tileTypeCount = 4;
    /// <summary>
    /// Storage for up to 5 textures
    /// </summary>
    public readonly Texture2D[] TileTextures;
    /// <summary>
    /// Custom Color Tints per texture
    /// </summary>
    public readonly Color[] TileTints;

    public ValueGridRenderer()
    {
        // Set storage
        TileTextures = new Texture2D[tileTypeCount + 1];
        TileTints = new Color[tileTypeCount + 1];

        // Load defaults
        //TileTextures[0] = 'Intentionally left blank'
        TileTextures[1] = LoadTextureFromImage(LoadImage("../Images/Wall.png"));
        TileTextures[2] = LoadTextureFromImage(LoadImage("../Images/Floor.png"));
        TileTextures[3] = LoadTextureFromImage(LoadImage("../Images/Corridor.png"));
        TileTextures[4] = LoadTextureFromImage(LoadImage("../Images/Floor.png"));

        // Set defaults
        // Tile tints are Raylib Color tints added at draw time
        TileTints[0] = Color.BLANK;
        TileTints[1] = Color.DARKGRAY;
        TileTints[2] = Color.BLUE;
        TileTints[3] = Color.YELLOW;
        TileTints[4] = Color.RED;

        // Average tile width (Incase source image is modded)
        TileResolutionX = 0;
        for (int index = 1; index < TileTextures.Length; ++index)
        {
            TileResolutionX += TileTextures[index].width;
        }
        TileResolutionX = TileResolutionX / tileTypeCount;

        // Average tile height (Incase source image is modded)
        TileResolutionY = 0;
        for (int index = 1; index < TileTextures.Length; ++index)
        {
            TileResolutionY += TileTextures[index].height;
        }
        TileResolutionY = TileResolutionY / tileTypeCount;
    }

    /// <summary>
    /// Draw the four primative tile types, no region shading
    /// </summary>
    /// <param name="a_valueGrid"></param>
    public void Draw(ValueGrid a_valueGrid)
    {
        int screenWidth = GetScreenWidth();
        int screenHeight = GetScreenHeight();

        // Calculate unscaled tile count
        HorizontalTileCount = screenWidth / TileResolutionX;
        VerticalTileCount = screenHeight / TileResolutionY;

        // Calculate Ratios on each axis
        float ratioOnX = HorizontalTileCount / (float)a_valueGrid.Width;
        float ratioOnY = VerticalTileCount / (float)a_valueGrid.Height;

        // Work out the final screen space pixel size of each tile
        scaleXY = Math.Min(ratioOnX, ratioOnY);
        sizeXY = scaleXY * Math.Max(TileResolutionX, TileResolutionY);

        // Loop through and draw
        Vector2 position = new Vector2(0,0);
        for (int X = 0; X < a_valueGrid.Width; X++)
        {
            for (int Y = 0; Y < a_valueGrid.Height; Y++)
            {
                position.x = sizeXY * X;
                position.y = sizeXY * Y;

                DrawTextureEx(TileTextures[a_valueGrid[X,Y]], position, 0.0f, scaleXY, TileTints[a_valueGrid[X,Y]]);
            }
        }
    }
}